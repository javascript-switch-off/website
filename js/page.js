// @licstart  The following is the entire license notice for the
//  JavaScript code in this page.
//
//  Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
//
//  The JavaScript code in this page is free software: you can
//  redistribute it and/or modify it under the terms of the GNU
//  General Public License (GNU GPL) as published by the Free Software
//  Foundation, either version 3 of the License, or (at your option)
//  any later version.  The code is distributed WITHOUT ANY WARRANTY
//  without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
//
//  As additional permission under GNU GPL version 3 section 7, you
//  may distribute non-source (e.g., minimized or compacted) forms of
//  that code without the copy of the GNU GPL normally required by
//  section 4, provided you include this license notice and a URL
//  through which recipients can access the Corresponding Source.
//
//  @licend  The above is the entire license notice
//  for the JavaScript code in this page.
//
// @source http://e-valkov.org/epiphany-extensions/js-switch-off/js/page.js

window.addEvent("domready", function() {
   // Piwik tracking
    // Destroy the static tracker
    try {
	$('piwik-image-tracker').destroy();
    } catch (err) {}

    var pkBaseURL = (("https:" == document.location.protocol) ?
		     "https://piwik.e-valkov.org/piwik/" :
		     "http://piwik.e-valkov.org/piwik/");
    var piwik_site_id = 2;

    var script = document.createElement("script");
    script.setAttribute('src', pkBaseURL+"piwik.js");
    script.setAttribute('defer', 'defer');
    script.setAttribute('async', 'async');

    $(script).inject($$('head')[0], "bottom");

    window.piwikTracker = null;
    window.piwik_timer = 0;
    var piwik_ticker = function() {
	try {
	    window.piwik_timer++;
	    window.piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", piwik_site_id);
	    var piwikTracker = window.piwikTracker;
	    piwikTracker.trackPageView();
	    piwikTracker.enableLinkTracking();
	    clearInterval(window.piwik_ticker_id);
	} catch(err) {}

	if (window.piwik_timer > 10) {
	    clearInterval(window.piwik_ticker_id);
	}
    }

    window.piwik_ticker_id = piwik_ticker.periodical(1000);

    // End of Piwik tracking

    $$('a[href^=#]').each(function (el) {
	el.addEvent('click', function (e) {
	    new Event(e).stop();
	    id = $(el).get('href').split('#')[1];
	    var pos = $(id).getPosition();

	    $(window).scrollTo(pos.x, (pos.y-60));  

	    $(id).set("tween", {duration: 500}).fade(0.2);
	    
	    (function() {
		$(id).set("tween", {duration: 500}).fade('in');
	    }).delay(1300);

	});
    });
});